import React, { Component } from 'react';
import './App.css';
import ListaVehiculos from './componentes/listaVehiculos/listaVehiculos'
import FormOrdenAlquiler from './componentes/formOrdenAlquiler/FormOrdenAlquiler'
import ListOrdenAlquiler from './componentes/listOrdenAlquiler/listOrdenAlquiler'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

//function App() {
class App extends Component  {
  constructor() {
    super();
  }

  render(){
    return (
      <Router>
          <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <div className="container">
            <Link className="navbar-brand" to="/">Inicio</Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
              <ul className="navbar-nav">
                <li className="nav-item active">
                <Link className="nav-link" to="/ordenes">Ordenes de alquiler</Link>
                </li>
              </ul>
            </div>
            </div>
          </nav>
          <div className="container p-4">
         <Route path="/" exact component={ListaVehiculos} / >
         <Route path="/form/:id" component={FormOrdenAlquiler} />
         <Route path="/ordenes/" component={ListOrdenAlquiler} />
         </div>
    </Router>

    );
  }

}

export default App;
