import React, { Component } from 'react';
import logo from '../../img/icon-car.png';
import './listaVehiculos.css';
import firebase from 'firebase';
import { DB } from '../../DBfirebase'
import 'firebase/database'
import { Link } from "react-router-dom";

class ListaVehiculos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      vehiculos: [ ]
    };
console.log('Conexion');

 if (!firebase.apps.length) { this.app = firebase.initializeApp(DB); }

  this.dbVehiculos = firebase.database().ref().child('vehiculos');

  }

 abrirOrdenAlquiler(key){
   console.log('Click formulario'+key);
 }

 componentDidMount(){
   const { vehiculos } = this.state;
   this.dbVehiculos.on('child_added', snap => { // child_added --value
       vehiculos.push({
         idVehiculo: snap.key,
         marca: snap.val().marca,
         modelo: snap.val().modelo,
         combustible: snap.val().combustible,
         pasajeros: snap.val().pasajeros,
         precioPorDia: snap.val().precioPorDia
       })
         this.setState({vehiculos});
   })
 }

  render(){
    return(
      <div className="container">
        <h1 className="center">Vehiculos de alquiler</h1>
        <div className="row">
          {
            this.state.vehiculos.map(vehiculo => {
              return(
                <Link to={"/form/" + vehiculo.idVehiculo}>
                  <div className='vehiculo' onClick={ () => this.abrirOrdenAlquiler(vehiculo.idVehiculo) }>

                      <img src={logo} className="logo-car" alt="logo-car" />
                      <label className="combustible">Combustible: {vehiculo.combustible }</label>
                      <label className="pasajeros" >Pasajeros: {vehiculo.pasajeros}</label>
                      <label className="marca">{vehiculo.marca}</label>
                      <label className="modelo">{vehiculo.modelo}</label>
                      <label className="precioPorDia">${vehiculo.precioPorDia}/Día</label>
                  </div>
                </Link>
              )
            })
          }
          </div>

          </div>
    )
  }

}

export default ListaVehiculos;
