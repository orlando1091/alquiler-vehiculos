import React, { Component } from 'react';
import logo from '../../img/icon-calendar.png';
import './listaOrdenAlquiler.css';
import firebase from 'firebase';
import { DB } from '../../DBfirebase'
import 'firebase/database'
import { Link } from "react-router-dom";

class ListOrdenAlquiler extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ordenes: [ ]
    };

 if (!firebase.apps.length) { this.app = firebase.initializeApp(DB); }
  this.dbOrdenes = firebase.database().ref().child('ordenAlquiler');
  }

 abrirOrdenAlquiler(key){
   console.log('Click formulario'+key);
 }

 componentDidMount(){
   const { ordenes } = this.state;
   this.dbOrdenes.on('child_added', snap => { // child_added --value
       ordenes.push({
         idVehiculo: snap.val().idVehiculo,
         nombre: snap.val().nombre,
         telefono: snap.val().telefono,
         email: snap.val().email,
         licencia: snap.val().licencia
       })
         this.setState({ordenes});
   })
 }

  render(){
    return(
      <div className="container">
        <h1 className="center">Nuevas Ordenes de alquiler</h1>
        <div className="row">
          {
            this.state.ordenes.map(orden => {
              return(
                  <div className='col-12 orden'>
                      <img src={logo} className="logo-calendar" alt="logo-calendar" />
                      <label className="nombre">{orden.nombre }</label>
                      <label className="email" >{orden.email}</label>
                      <label className="telefono">Telefono: {orden.telefono}</label>
                      <label className="licencia">Licencia: {orden.licencia}</label>
                      <label className="idVehiculo">ID Vehiculo: {orden.idVehiculo}</label>
                  </div>
              )
            })
          }
          </div>

          </div>
    )
  }

}

export default ListOrdenAlquiler;
