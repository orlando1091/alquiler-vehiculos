import React, { Component } from 'react';
import firebase from 'firebase';
import { DB } from '../../DBfirebase'
import 'firebase/database'


class FormOrdenAlquiler extends Component {
  constructor(props) {
    super(props);
    this.state = {
      idVehiculo:''
    };

    this.dbOrdenAlquiler = firebase.database().ref().child('ordenAlquiler');
  }

  componentDidMount(){
    console.log(this.props.match.params.id);
    this.setState({
      idVehiculo: this.props.match.params.id
    })
  }

  enviarForm(){
    this.dbOrdenAlquiler.push().set({nombre:this.nombre.value,
                                 telefono: this.telefono.value,
                                 email: this.email.value,
                                 licencia: this.licencia.value,
                                 idVehiculo: this.state.idVehiculo})
  window.location.href = '/ordenes/';
    }

  render(){
    return(
      <div className="container">
        <h1 className="center">Orden de alquiler</h1>
            <div className='FormOrdenAlquiler'>
                  <div className='form-group'>
                    <label >Nombre</label>
                    <input
                     ref = {input => { this.nombre = input; }}
                     type='email' className='form-control' id='nombre' />
                  </div>
                  <div className='form-group'>
                    <label >Email</label>
                    <input
                     ref = {input => { this.email = input; }}
                     type='email' className='form-control' id='email' aria-describedby='emailHelp'/>
                  </div>
                  <div className='form-group'>
                    <label >Telefono</label>
                    <input
                     ref = {input => { this.telefono = input; }}
                     type='email' className='form-control' id='telefono' />
                  </div>
                  <div className='form-group'>
                    <label >Licencia</label>
                    <input
                     ref = {input => { this.licencia = input; }}
                     type='email' className='form-control' id='licencia' />
                  </div>
                  <button className='btn btn-primary' onClick={ () => this.enviarForm() }>Confirmar solicitud</button>
            </div>
       </div>
    )
  }

}

export default FormOrdenAlquiler;
